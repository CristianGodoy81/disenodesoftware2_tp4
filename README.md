Trabajo Practico Nº4

Ejercicio: Se debe proponer una solución aplicando el paradigma orientado a objetos, en la cual se debe aplicar los conceptos de métodos constructores, herencia, polimorfismo, clases abstractas y/o interfaces según corresponda. Además, se pide:

i. Realizar el listado de clases candidatas y también el listado de clases candidatas
resultante de haber realizado el análisis y depuración de listado de clases candidatas explicando brevemente porque desecha las clases excluidas.
ii. Realizar el diagrama de clases de la solución inicial o diagrama de dominio.
iii. Codificar la solución propuesta en el diagrama de clase empleando el lenguaje java.
iv. Codificar una clase principal y crear instancias de los distintos objetos para probar
la funcionalidad de la solución.

1) Se debe programar una aplicación para poder calcular tanto el precio de alquiler como de
venta de los vehículos con que cuenta una empresa que se dedica a alquilar vehículos tanto
de pasajeros como de carga, y también se dedica a la venta de autos 0 km y usados. De
los vehículos se almacena marca, patente, el precio base de alquiler o de venta según
corresponda. Los vehículos de alquiler pueden ser autos, minibús, camionetas y camiones.
El precio de los autos 0 km se compone de un precio base más el valor correspondiente a
los componentes extras que se dese agregar, (aire acondicionado 2%, levanta cristales
eléctricos 5%, alarma un 1%), más el 50% de utilidad. El precio de los usados se obtiene
a partir del precio base más un 35% de utilidad. Los autos 0 km no se alquilan en cambio
los autos usados sí. Los autos se deben registrar al ingresar al parque automotor para estar
disponibles para alquilar o vender. El precio de alquiler se compone de un precio base por
día. En el caso de coches se suma de $50 por y plaza por día. El precio de alquiler del
microbús es igual al de los coches más $250 de seguro por plaza. El precio de alquiler de
los vehículos de carga es de $300 si el viaje es menor a 50 Km. En caso contrario su precio
será de $20 multiplicado por kilómetro recorrido. Para los camiones se debe abonar $200
extras independientemente del kilometraje recorrido. se debe mostrar el pecio obtenido
para cada caso. 