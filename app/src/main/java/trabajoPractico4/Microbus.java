package trabajoPractico4;

public class Microbus extends Vehiculo implements AlquilerDeVehiculoDePasajeros{
    public static final Double porcentajeDeUtilidad = 35.00;
    public static final Double valorPorPlaza = 50.00;
    public static final Double valorPorDia = 50.00;
    public static final Double valorSeguroPorPlaza = 250.00;
    // Atributos
    // Metodos
    @Override
    public Double precioAlquiler(Integer diasDeAlquiler){
        return ((this.getPrecioBase())+(valorPorPlaza*this.getPlazas())+(valorPorDia*diasDeAlquiler)+(valorSeguroPorPlaza*this.getPlazas()));
    }
    public String precioVenta(){
        return "Los microbuses no se venden";
    }
    @Override
    public String toString(){
        return (
            "\nMicrobus:"+
            "\n\tMarca: "+getMarca()+
            "\n\tPatente: "+getPatente()+
            "\n\tCantidad de plazas: "+getPlazas()+"\n"
        );
    }
    // Constructor
    public Microbus(String marca, String patente, Double precioBaseAlquiler, Integer plaza){
        super(marca, patente, precioBaseAlquiler, plaza);
    }
    // Getters
    // Setters
}
