package trabajoPractico4;

public interface AlquilerDeVehiculoDePasajeros {
    // Atributos
    // Metodos
    public abstract Double precioAlquiler(Integer diasDeAlquiler); // No es necesario colocar "public abstract".
}
