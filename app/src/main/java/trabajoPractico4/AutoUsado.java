package trabajoPractico4;

public class AutoUsado extends Vehiculo implements AlquilerDeVehiculoDePasajeros, VentaDeVehiculo{
    public static final Double porcentajeDeUtilidad = 35.00;
    public static final Double valorPorPlaza = 50.00;
    public static final Double valorPorDia = 50.00;
    // Atributos
    Double precioBaseAlquiler;
    // Metodos
    @Override
    public Double precioAlquiler(Integer diasDeAlquiler){
        return ((this.precioBaseAlquiler)+(valorPorPlaza*this.getPlazas())+(valorPorDia*diasDeAlquiler));
    }
    @Override
    public Double precioVenta(){
        return this.getPrecioBase()+utilidad();
    }
    private Double utilidad(){
        return ((this.getPrecioBase()*porcentajeDeUtilidad)/100);
    }
    @Override
    public String toString(){
        return (
            "\nAuto usado:"+
            "\n\tMarca: "+getMarca()+
            "\n\tPatente: "+getPatente()+
            "\n\tCantidad de plazas: "+getPlazas()+"\n"
        );
    }
    // Constructor
    public AutoUsado(String marca, String patente, Double precioBaseAlquiler, Double precioBaseVenta, Integer plazas){
        super(marca, patente, precioBaseVenta, plazas);
        this.precioBaseAlquiler = precioBaseAlquiler;
    }
    // Getters
    public Double getPrecioBaseDeAlquiler(){
        return precioBaseAlquiler;
    }
    // Setters
    public void setPrecioBaseDeAlquiler(Double precioBaseDeAlquiler){
        this.precioBaseAlquiler = precioBaseDeAlquiler;
    }
}
