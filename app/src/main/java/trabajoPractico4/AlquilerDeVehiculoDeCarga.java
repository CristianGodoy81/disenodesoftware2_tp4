package trabajoPractico4;

public interface AlquilerDeVehiculoDeCarga {
    // Atributos
    // Metodos
    public abstract Double precioAlquiler(Integer kmRecorrido); // No es necesario colocar "public abstract".
}
