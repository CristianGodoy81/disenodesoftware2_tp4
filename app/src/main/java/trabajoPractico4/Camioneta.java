package trabajoPractico4;

public class Camioneta extends Vehiculo implements AlquilerDeVehiculoDeCarga{
    public static final Double kilometros = 50.0;
    public static final Double cargoFijoMenorA50Km = 300.0;
    public static final Double cargoFijoMayorA50Km = 20.0; // Por kilometro recorrido.
    // Atributos
    // Metodos
    @Override
    public Double precioAlquiler(Integer kmRecorrido){
        if(kmRecorrido < kilometros){
            return ((this.getPrecioBase())+(cargoFijoMenorA50Km));
        }else{
            return ((this.getPrecioBase())+(cargoFijoMenorA50Km*kmRecorrido));
        }
    }
    public String precioVenta(){
        return "Las camionetas no se venden";
    }
    @Override
    public String toString(){
        return (
            "\nCamioneta:"+
            "\n\tMarca: "+getMarca()+
            "\n\tPatente: "+getPatente()+
            "\n\tCantidad de plazas: "+getPlazas()+"\n"
        );
    }
    // Constructor
    public Camioneta(String marca, String patente, Double precioBaseAlquiler, Integer plazas){
        super(marca, patente, precioBaseAlquiler, plazas);
    }
    // Getters
    // Setters
}
