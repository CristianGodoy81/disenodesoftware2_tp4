package trabajoPractico4;

import java.util.ArrayList;

public class ParqueAutomotor {
    // Atributos
    private String nombre;
    private ArrayList<Vehiculo> vehiculos;
    // Metodos
    // Constructor
    ParqueAutomotor(String nombre){
        this.nombre = nombre;
        this.vehiculos = new ArrayList<Vehiculo>();
    }
    // Getters
    String getNombre(){
        return nombre;
    }
    ArrayList<Vehiculo> getVehiculos(){
        return vehiculos;
    }
}
