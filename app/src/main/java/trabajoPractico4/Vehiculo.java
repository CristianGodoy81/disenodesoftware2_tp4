package trabajoPractico4;

public abstract class Vehiculo {
    // Atributos
    private String marca;
    private String patente;
    private Double precioBase;
    private Integer plazas;
    // Metodos
    // Constructor
    public Vehiculo(String marca, String patente, Double precioBase, Integer plazas){
        this.marca = marca;
        this.patente = patente;
        this.precioBase = precioBase;
        this.plazas = plazas;
    }
    // Getters
    public String getMarca(){
        return marca;
    }
    public String getPatente(){
        return patente;
    }
    public Double getPrecioBase(){
        return precioBase;
    }
    public Integer getPlazas(){
        return plazas;
    }
    // Setters
    public void setMarca(String marca){
        this.marca = marca;
    }
    public void setPatente(String patente){
        this.patente = patente;
    }
    public void setPrecioBase(Double precioBase){
        this.precioBase = precioBase;
    }
    public void setPlazas(Integer plazas){
        this.plazas = plazas;
    }
}
