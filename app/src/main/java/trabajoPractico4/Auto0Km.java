package trabajoPractico4;

public class Auto0Km extends Vehiculo implements VentaDeVehiculo{
    public static final Double porcentajeDeUtilidad = 50.0;
    public static final Double porcentajePorAireA = 2.0;
    public static final Double porcentajePorLevantaC = 5.0;
    public static final Double porcentajePorAlarma = 1.0;
    // Atributos
    private Boolean aireAcondicionado;
    private Boolean levantaCristales;
    private Boolean alarma;
    // Metodos
    @Override
    public Double precioVenta(){
        return (getPrecioBase()+extraPorAireA()+extraPorLevantaCristales()+extraPorAlarma()+utilidad());
    }
    public String precioAlquiler(){
        return "Los autos 0Km no se alquilan";
    }
    public String precioAlquiler(Integer x){
        return "Los autos 0Km no se alquilan";
    }
    private Double utilidad(){
        return (((this.getPrecioBase()+extraPorAireA()+extraPorLevantaCristales()+extraPorAlarma())*porcentajeDeUtilidad)/100);
    }
    private Double extraPorAireA(){
        if(this.aireAcondicionado){
            return ((this.getPrecioBase()*porcentajePorAireA)/100);
        }else{
            return 0.0;
        }
    }
    private Double extraPorLevantaCristales(){
        if(this.levantaCristales){
            return ((this.getPrecioBase()*porcentajePorLevantaC)/100);
        }else{
            return 0.0;
        }
    }
    private Double extraPorAlarma(){
        if(this.alarma){
            return ((this.getPrecioBase()*porcentajePorAlarma)/100);
        }else{
            return 0.0;
        }
    }
    @Override
    public String toString(){
        return (
            "\nAuto 0km:"+
            "\n\tMarca: "+this.getMarca()+
            "\n\tPatente: "+this.getPatente()+
            "\n\tCantidad de plazas: "+this.getPlazas()+
            "\n\tAire Acondicionado: "+this.getAireAcondicionado()+
            "\n\tLevanta cristales: "+this.getLevantaCristales()+
            "\n\tAlarma: "+this.getAlarma()+"\n"
        );
    }
    // Constructor
    public Auto0Km(String marca, String patente, Double precioBaseVenta, Integer plazas, Boolean aireAcondicionado, Boolean levantaCristales, Boolean alarma){
        super(marca, patente, precioBaseVenta, plazas);
        this.aireAcondicionado = aireAcondicionado;
        this.levantaCristales = levantaCristales;
        this.alarma = alarma;
    }
    // Getters
    public Boolean getAireAcondicionado(){
        return this.aireAcondicionado;
    }
    public Boolean getLevantaCristales(){
        return levantaCristales;
    }
    public Boolean getAlarma(){
        return alarma;
    }
    // Setters
    public void setAireAcondicionado(Boolean aireAcondicionado){
        this.aireAcondicionado = aireAcondicionado;
    }
    public void setLevantaCristales(Boolean levantaCristales){
        this.levantaCristales = levantaCristales;
    }
    public void setAlarma(Boolean alarma){
        this.alarma = alarma;
    }
}
